import requests
import json
import logging
import config
import os
import random

import datetime

from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater, CommandHandler


def get_weekday(time):
    """Returns the weekday number from time"""
    for i in range(7):
        if time in config.weekdays[i]:
            return i
    return None


def get_fixed_name_location(city):

    """Tries to find the location of the city, using Yandex Geocoder.
    Returns description and location if the city was found else None
    """

    params = {
        'geocode': city,
        'format': 'json'
    }
    while True:
        try:
            rs = requests.post(url=config.yandex_geoapi, data=params)
            break
        except requests.RequestException as error:
            logging.error(error)

    if rs.status_code != 200:
        logging.error(rs.reason)
        return None

    data = json.loads(rs.text)
    for geoobject in data['response']['GeoObjectCollection']['featureMember']:
        member = geoobject['GeoObject']
        meta_data = member['metaDataProperty']['GeocoderMetaData']
        if meta_data['kind'] == 'locality' or meta_data['kind'] == 'province':
            coords = member['Point']['pos'].split()
            location = coords[1] + "," + coords[0]
            return meta_data['text'], location
    return None


def get_location(city):

    """Tries to modify the city's name by adding prefix and removing suffix.
    After that gets the location of the city, using get_fixed_name_location.
    """

    names = [city]
    if len(city) > 1:
        names.append(city[:-1])
    if len(city) > 2:
        names.append(city[:-2])
    prefixes = ['г. ', 'c. ', 'п. ', 'д. ', '']
    for prefix in prefixes:
        for name in names:
            result = get_fixed_name_location(prefix + name)
            if result is not None:
                return result
    return None


def get_weather(location, date=None, num_of_days=1):

    """Pure function to work with World Weather API.
    Takes location of the city, date and the number of days,
    returns the response of API.
    """

    params = {
        'q': location,
        'format': 'json',
        'key': config.weather_key,
        'lang': 'ru',
        'num_of_days': num_of_days,
        'tp': 12
    }
    if date is not None:
        params['date'] = date
    while True:
        try:
            rs = requests.post(url=config.weather_api, data=params)
            break
        except requests.RequestException as error:
            logging.error(error)
    if rs.status_code != 200:
        logging.error(rs.reason)
        return None
    # print(json.loads(rs.text)['data'])
    return json.loads(rs.text)['data']


def send_day_weather(bot, chat_id, weather):
    """Generates and sends message, containing weather on the future day."""
    date = weather['date']
    weekday = datetime.datetime(int(date[:4]), int(date[5:7]), int(date[8:10]))
    header = 'Прогноз погоды {}, {}:\n\n'.format(
        config.weekdays[weekday.weekday()][1], date
    )
    temperature = "{} — {}".format(weather['mintempC'],
                                   weather['maxtempC'])
    text = weather_to_message(weather['hourly'][0], temperature=temperature)
    bot.send_message(chat_id=chat_id, text=(header + text))


def send_current_weather(bot, chat_id, weather):
    """Generates and sends message, containing weather on the current day."""
    header = 'Текущий прогноз погоды:\n\n'
    text = weather_to_message(weather)
    bot.send_message(chat_id=chat_id, text=(header + text))


def send_weather_by_time(bot, chat_id, location, time):

    """A part of bot functionality, takes location and time,
    parses time and sends correct weather. If response contains only
    one day, then also send a poem, using send_poem function.
    """

    if time == 'сегодня' or time == 'сейчас' or time == '':
        weather = get_weather(location, 'today')
        send_current_weather(bot, chat_id, weather['current_condition'][0])
        send_poem(bot, chat_id, weather['current_condition'][0]['weatherCode'])
    elif time == 'завтра':
        weather = get_weather(location, 'tomorrow')
        send_day_weather(bot, chat_id, weather['weather'][0])
        send_poem(bot, chat_id,
                  weather['weather'][0]['hourly'][0]['weatherCode'])
    elif time == 'послезавтра':
        today = datetime.date.today() + datetime.timedelta(2)
        weather = get_weather(location, str(today))
        send_day_weather(bot, chat_id, weather['weather'][0])
        send_poem(bot, chat_id,
                  weather['weather'][0]['hourly'][0]['weatherCode'])
    elif 'выходные' in time.split():
        today = datetime.date.today()
        while today.weekday() != 5:
            today += datetime.timedelta(1)
        weather = get_weather(location, str(today))
        send_day_weather(bot, chat_id, weather['weather'][0])

        today += datetime.timedelta(1)
        weather = get_weather(location, str(today))
        send_day_weather(bot, chat_id, weather['weather'][0])
    elif get_weekday(time) is not None:
        weekday = get_weekday(time)
        today = datetime.date.today() + datetime.timedelta(1)
        while today.weekday() != weekday:
            today += datetime.timedelta(1)

        weather = get_weather(location, str(today))
        send_day_weather(bot, chat_id, weather['weather'][0])
        send_poem(bot, chat_id,
                  weather['weather'][0]['hourly'][0]['weatherCode'])
    elif time in config.counting_days:
        period = config.counting_days[time]
        weather = get_weather(location, num_of_days=period)
        for i in range(period):
            send_day_weather(bot, chat_id, weather['weather'][i])
    else:
        try:
            date = datetime.datetime.strptime(time, '%Y-%m-%d')
            date = datetime.date(date.year, date.month, date.day)
            weather = get_weather(location, str(date))
            send_day_weather(bot, chat_id, weather['weather'][0])
            send_poem(bot, chat_id,
                      weather['weather'][0]['hourly'][0]['weatherCode'])
        except ValueError:
            weather = get_weather(location, 'today')
            send_current_weather(bot, chat_id, weather['current_condition'][0])
            send_poem(bot, chat_id,
                      weather['current_condition'][0]['weatherCode'])


def send_poem(bot, chat_id, weather_code):
    """Sends poem using weather code."""
    for name in config.weather_codes:
        if int(weather_code) in config.weather_codes[name]:
            path = config.poems_path + name + '/'
            file_list = os.listdir(path)
            file_name = random.choice(file_list)
            with open(path + file_name) as markdown:
                bot.send_message(chat_id=chat_id, text=markdown.read(),
                                 parse_mode='Markdown')
            return
    path = config.poems_path + 'погода' + '/'
    file_list = os.listdir(path)
    file_name = random.choice(file_list)
    with open(path + file_name) as markdown:
        bot.send_message(chat_id=chat_id, text=markdown.read(),
                         parse_mode='Markdown')


def parse_message(text):
    """This function parse pure message text to location and time."""
    text = text.lower().split()
    words = []
    for word in text:
        if len(word) > 0:
            words.append(word)
    if len(words) >= 3 and ' '.join(words[:3]) == 'какая погода в':
        words = words[3:]
    elif len(words) >= 2 and ' '.join(words[:2]) == 'погода в':
        words = words[2:]
    elif len(words) >= 2 and ' '.join(words[:2]) == 'какая погода':
        words = words[2:]
    elif len(words) >= 1 and ' '.join(words[:1]) == 'погода':
        words = words[1:]

    if not len(words):
        return None

    city = words[0][:50]
    time = ' '.join(words[1:])[:50]
    return city, time


def get_first_image(city):
    """Returns url of the first found picture of qwant api response."""
    headers = {
        'User-Agent': config.user_agent
    }
    params = {
        'q': city,
        'count': 1,
        'offset': 1,
        'local': 'ru_ru'
    }
    rs = requests.get(config.qwant_api, params=params, headers=headers)
    obj = json.loads(rs.content)
    return obj['data']['result']['items'][0]['media']


def weather_to_message(weather, temperature=None):
    """Converts weather object to human-readable text."""
    if temperature is None:
        temperature = weather['temp_C']
    result = weather['lang_ru'][0]['value'] + '\n'
    result += 'Температура воздуха {}°C\n'.format(temperature)
    result += 'Ветер {}, {} м/с\n'.format(
        config.wind_direction[weather['winddir16Point']],
        int(float(weather['windspeedKmph']) / 3.6)
    )
    result += 'Атмосферное давление {} мм рт. ст.\n'.format(
        int(float(weather['pressure']) * 0.75)
    )
    return result


def bot_send_weather(bot, update):
    """This function describes the actions of the bot
    in response to a query about the weather.
    """
    # print(update)

    message = update.message
    chat_id = message.chat_id
    parse_data = parse_message(message.text)
    if parse_data is None:
        bot.send_message(chat_id=chat_id, text='Не понял ваш запрос. ' +
                                               'Воспользуйтесь командой /help')
        return

    city, time = parse_data
    location = get_location(city)
    if location is None:
        bot.send_message(chat_id=chat_id, text="Не могу найти ваш город.")
        return
    bot.send_message(chat_id=chat_id, text=location[0])
    bot.send_message(chat_id=chat_id, text='Геокоординаты: ' + location[1])
    bot.send_photo(chat_id=chat_id,
                   photo=get_first_image(location[0].split()[-1]))
    send_weather_by_time(bot, chat_id, location[1], time)


def bot_help(bot, update):
    with open(config.help_path) as help_md:
        bot.send_message(chat_id=update.message.chat_id, text=help_md.read(),
                         parse_mode='Markdown')


if __name__ == "__main__":
    logging.basicConfig(filename='logs.log')
    updater = Updater(token=config.telegram_token)
    dispatcher = updater.dispatcher
    weather_handler = MessageHandler(Filters.text & Filters.private,
                                     bot_send_weather)
    dispatcher.add_handler(weather_handler)
    help_handler = CommandHandler('help', bot_help)
    dispatcher.add_handler(help_handler)
    updater.start_polling()
