# Api address of Local Weather API
weather_api = 'http://api.worldweatheronline.com/premium/v1/weather.ashx'
# Local Weather API key
weather_key = ''
# Yandex geolocation API address
yandex_geoapi = 'https://geocode-maps.yandex.ru/1.x/'
# Qwant api address -- api for image searching
qwant_api = 'https://api.qwant.com/api/search/images'
# Telegram bot token
telegram_token = ''

# Translations of wind directions from 16 point compass
# to 8 point russian wind direction names
wind_direction = {
    'N': 'северный',
    'NNE': 'северный',
    'NE': 'северо-восточный',
    'ENE': 'северо-восточный',
    'E': 'восточный',
    'ESE': 'восточный',
    'SE': 'юго-восточный',
    'SSE': 'юго-восточный',
    'S': 'южный',
    'SSW': 'южный',
    'SW': 'юго-западный',
    'WSW': 'юго-западный',
    'W': 'западный',
    'WNW': 'западный',
    'NW': 'северо-западный',
    'NNW': 'северо-западный',
}

# Russian weekday names in different forms
weekdays = [
    ['понедельник', 'на понедельник', 'в понедельник'],
    ['вторник', 'на вторник', 'в воскресенье'],
    ['среда', 'на среду', 'в среду'],
    ['четверг', 'на четверг', 'в четверг'],
    ['пятница', 'на пятницу', 'в пятницу'],
    ['суббота', 'на субботу', 'в субботу'],
    ['воскресенье', 'на воскресенье', 'в воскресенье'],
]

# The length of the intervals of days.
counting_days = {
    'на два дня': 2,
    'на три дня': 3,
    'на четыре дня': 4,
    'на пять дней': 5,
    'на неделю': 7,
    'на неделе': 7,
}

# Сlassification of weather codes from
# https://developer.worldweatheronline.com/api/docs/weather-icons.aspx
weather_codes = {
    'снег': [371, 368, 365, 362, 338, 335, 332, 329, 326,
             323, 277, 179, 320, 317, 314, 311, ],
    'дождь': [359, 356, 353, 308, 305, 302, 299, 296, 293,
              266, 263, 176, 284, 281, 185, 182, ],
    'гроза': [395, 392, 386, 389, 200],
    'метель': [230, ],
    'пасмурно': [119, 116, 122, 260, 248, 143, ],
    'солнечно': [113, ],
}

# Some default web-browser user agent
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) ' + \
             'AppleWebKit/537.36 (KHTML, like Gecko) ' +\
             'Chrome/39.0.2171.95 Safari/537.36'

# Absolute path to the bots folder
bots_path = '/home/kronecker/Programming/SHAD/python/WeatherBot/'

# Absolute path to the help.md
help_path = bots_path + 'help/help.md'

# Absolute path to the poems
poems_path = bots_path + 'poems/'
